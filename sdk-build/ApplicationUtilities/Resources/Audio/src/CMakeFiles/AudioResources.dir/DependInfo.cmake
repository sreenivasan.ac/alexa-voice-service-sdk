# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/src/AlertsAudioFactory.cpp" "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/AlertsAudioFactory.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/src/AudioFactory.cpp" "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/AudioFactory.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/src/CommunicationsAudioFactory.cpp" "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/CommunicationsAudioFactory.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/src/NotificationsAudioFactory.cpp" "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/NotificationsAudioFactory.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
