# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/KWD/test
# Build directory: /Users/h/sdk-folder/sdk-build/KWD/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(AbstractKeyWordDetectorTest.testAddKeyWordObserver "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testAddKeyWordObserver")
add_test(AbstractKeyWordDetectorTest.testAddMultipleKeyWordObserver "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testAddMultipleKeyWordObserver")
add_test(AbstractKeyWordDetectorTest.testRemoveKeyWordObserver "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testRemoveKeyWordObserver")
add_test(AbstractKeyWordDetectorTest.testAddStateObserver "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testAddStateObserver")
add_test(AbstractKeyWordDetectorTest.testAddMultipleStateObservers "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testAddMultipleStateObservers")
add_test(AbstractKeyWordDetectorTest.testRemoveStateObserver "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testRemoveStateObserver")
add_test(AbstractKeyWordDetectorTest.testObserversDontGetNotifiedOfSameStateTwice "/Users/h/sdk-folder/sdk-build/KWD/test/AbstractKeywordDetectorTest" "--gtest_filter=AbstractKeyWordDetectorTest.testObserversDontGetNotifiedOfSameStateTwice")
