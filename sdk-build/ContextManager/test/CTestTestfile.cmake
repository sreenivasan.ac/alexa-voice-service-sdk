# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/ContextManager/test
# Build directory: /Users/h/sdk-folder/sdk-build/ContextManager/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(ContextManagerTest.testSetStateForRegisteredProvider "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testSetStateForRegisteredProvider")
add_test(ContextManagerTest.testSetStateForUnregisteredProvider "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testSetStateForUnregisteredProvider")
add_test(ContextManagerTest.testSetStateForUnregisteredProviderWithRefreshPolicyAlways "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testSetStateForUnregisteredProviderWithRefreshPolicyAlways")
add_test(ContextManagerTest.testGetContext "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testGetContext")
add_test(ContextManagerTest.testMultipleGetContextRequests "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testMultipleGetContextRequests")
add_test(ContextManagerTest.testSetProviderTwice "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testSetProviderTwice")
add_test(ContextManagerTest.testProvideStateTimeout "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testProvideStateTimeout")
add_test(ContextManagerTest.testRemoveProvider "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testRemoveProvider")
add_test(ContextManagerTest.testIncorrectToken "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testIncorrectToken")
add_test(ContextManagerTest.testEmptyProvider "/Users/h/sdk-folder/sdk-build/ContextManager/test/ContextManagerTest" "--gtest_filter=ContextManagerTest.testEmptyProvider")
