# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/PlaylistParser/test
# Build directory: /Users/h/sdk-folder/sdk-build/PlaylistParser/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(PlaylistParserTest.testEmptyUrl "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testEmptyUrl")
add_test(PlaylistParserTest.testNullObserver "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testNullObserver")
add_test(PlaylistParserTest.testParsingPlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingPlaylist")
add_test(PlaylistParserTest.testParsingRelativePlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingRelativePlaylist")
add_test(PlaylistParserTest.testParsingHlsPlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingHlsPlaylist")
add_test(PlaylistParserTest.testParsingPlsPlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingPlsPlaylist")
add_test(PlaylistParserTest.testParsingRecursiveHlsPlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingRecursiveHlsPlaylist")
add_test(PlaylistParserTest.testNotParsingCertainPlaylistTypes "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testNotParsingCertainPlaylistTypes")
add_test(PlaylistParserTest.testParsingLiveStreamPlaylist "/Users/h/sdk-folder/sdk-build/PlaylistParser/test/PlaylistParserTest" "--gtest_filter=PlaylistParserTest.testParsingLiveStreamPlaylist")
