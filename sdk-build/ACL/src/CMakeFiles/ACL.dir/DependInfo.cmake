# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/AVSConnectionManager.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/AVSConnectionManager.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/HTTP2Stream.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/HTTP2Stream.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/HTTP2StreamPool.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/HTTP2StreamPool.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/HTTP2Transport.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/HTTP2Transport.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/HTTP2TransportFactory.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/HTTP2TransportFactory.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/MessageRouter.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/MessageRouter.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/MimeParser.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/MimeParser.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/PostConnectSynchronizer.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/PostConnectSynchronizer.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/PostConnectSynchronizerFactory.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/PostConnectSynchronizerFactory.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src/Transport/TransportDefines.cpp" "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/Transport/TransportDefines.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ACSDK_LOG_MODULE=acl"
  "ACSDK_OPENSSL_MIN_VER_REQUIRED=1.0.2"
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/MultipartParser"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
