# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/test/Transport/MimeParserTest.cpp" "/Users/h/sdk-folder/sdk-build/ACL/test/CMakeFiles/MimeParserTest.dir/Transport/MimeParserTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/MultipartParser"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googletest/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googlemock/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ACL/test/Transport/Common/CMakeFiles/ACLTransportCommonTestLib.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/CMakeFiles/gmock_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
