# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/AttachmentReaderSource.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/AttachmentReaderSource.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/BaseStreamSource.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/BaseStreamSource.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/ErrorTypeConversion.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/ErrorTypeConversion.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/IStreamSource.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/IStreamSource.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/MediaPlayer.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/MediaPlayer.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/Normalizer.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/Normalizer.cpp.o"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/src/OffsetManager.cpp" "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/OffsetManager.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ACSDK_LOG_MODULE=mediaPlayer"
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/include"
  "/usr/local/Cellar/gst-plugins-base/1.14.1/include/gstreamer-1.0"
  "/usr/local/Cellar/gstreamer/1.14.1/include/gstreamer-1.0"
  "/usr/local/Cellar/glib/2.56.1/include/glib-2.0"
  "/usr/local/Cellar/glib/2.56.1/lib/glib-2.0/include"
  "/usr/local/opt/gettext/include"
  "/usr/local/Cellar/pcre/8.42/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/PlaylistParser/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/PlaylistParser/src/CMakeFiles/PlaylistParser.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
