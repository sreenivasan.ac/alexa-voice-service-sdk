# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/PlaybackController/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(PlaybackControllerTest.createSuccessfully "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.createSuccessfully")
add_test(PlaybackControllerTest.createWithError "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.createWithError")
add_test(PlaybackControllerTest.playButtonPressed "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.playButtonPressed")
add_test(PlaybackControllerTest.pauseButtonPressed "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.pauseButtonPressed")
add_test(PlaybackControllerTest.nextButtonPressed "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.nextButtonPressed")
add_test(PlaybackControllerTest.previousButtonPressed "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.previousButtonPressed")
add_test(PlaybackControllerTest.getContextFailure "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.getContextFailure")
add_test(PlaybackControllerTest.sendMessageFailure "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.sendMessageFailure")
add_test(PlaybackControllerTest.sendMessageException "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackControllerTest" "--gtest_filter=PlaybackControllerTest.sendMessageException")
add_test(PlaybackRouterTest.defaultHandler "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackRouterTest" "--gtest_filter=PlaybackRouterTest.defaultHandler")
add_test(PlaybackRouterTest.secondHandler "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackRouterTest" "--gtest_filter=PlaybackRouterTest.secondHandler")
add_test(PlaybackRouterTest.switchToDefaultHandler "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/test/PlaybackRouterTest" "--gtest_filter=PlaybackRouterTest.switchToDefaultHandler")
