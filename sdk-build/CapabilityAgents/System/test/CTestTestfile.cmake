# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/System/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(EndpointHandlerTest.createSuccessfully "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/EndpointHandlerTest" "--gtest_filter=EndpointHandlerTest.createSuccessfully")
add_test(EndpointHandlerTest.createWithError "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/EndpointHandlerTest" "--gtest_filter=EndpointHandlerTest.createWithError")
add_test(EndpointHandlerTest.handleDirectiveProperly "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/EndpointHandlerTest" "--gtest_filter=EndpointHandlerTest.handleDirectiveProperly")
add_test(SoftwareInfoSenderTest.createFailedInvalidFirmwareVersion "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createFailedInvalidFirmwareVersion")
add_test(SoftwareInfoSenderTest.createSuccessWithsendSoftwareInfoUponConnectFalse "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createSuccessWithsendSoftwareInfoUponConnectFalse")
add_test(SoftwareInfoSenderTest.createSuccessWithObserverNull "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createSuccessWithObserverNull")
add_test(SoftwareInfoSenderTest.createFailedConnectionNull "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createFailedConnectionNull")
add_test(SoftwareInfoSenderTest.createFailedMessageSenderNull "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createFailedMessageSenderNull")
add_test(SoftwareInfoSenderTest.createFailedExceptionEncounteredSenderNull "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.createFailedExceptionEncounteredSenderNull")
add_test(SoftwareInfoSenderTest.noSoftwareInfoEventSentByDefault "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.noSoftwareInfoEventSentByDefault")
add_test(SoftwareInfoSenderTest.nothingSentBeforeConnected "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.nothingSentBeforeConnected")
add_test(SoftwareInfoSenderTest.softwareInfoSentUponConnectIfSendSetTrueBeforeConnect "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.softwareInfoSentUponConnectIfSendSetTrueBeforeConnect")
add_test(SoftwareInfoSenderTest.reportSoftwareInfoReceived "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.reportSoftwareInfoReceived")
add_test(SoftwareInfoSenderTest.reportSoftwareInfoCancellsPreviousDirective "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.reportSoftwareInfoCancellsPreviousDirective")
add_test(SoftwareInfoSenderTest.delayedReportSoftwareInfoNotifiesOnce "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.delayedReportSoftwareInfoNotifiesOnce")
add_test(SoftwareInfoSenderTest.verifySendRetries "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.verifySendRetries")
add_test(SoftwareInfoSenderTest.setInvalidFirmwareVersion "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.setInvalidFirmwareVersion")
add_test(SoftwareInfoSenderTest.setFirmwareVersionCancellsPreviousSetting "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/SoftwareInfoTest" "--gtest_filter=SoftwareInfoSenderTest.setFirmwareVersionCancellsPreviousSetting")
add_test(UserInactivityMonitorTest.createSuccessfully "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.createSuccessfully")
add_test(UserInactivityMonitorTest.createWithError "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.createWithError")
add_test(UserInactivityMonitorTest.handleDirectiveProperly "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.handleDirectiveProperly")
add_test(UserInactivityMonitorTest.sendMultipleReports "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.sendMultipleReports")
add_test(UserInactivityMonitorTest.verifyInactivityTime "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.verifyInactivityTime")
add_test(UserInactivityMonitorTest.sendMultipleReportsWithReset "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/test/UserInactivityMonitorTest" "--gtest_filter=UserInactivityMonitorTest.sendMultipleReportsWithReset")
