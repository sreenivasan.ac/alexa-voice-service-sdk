# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/Alerts/test/AlertSchedulerTest.cpp" "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Alerts/test/CMakeFiles/AlertSchedulerTest.dir/AlertSchedulerTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/test"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/test"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/Alerts/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/RegistrationManager/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/test/Common"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googletest/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googlemock/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Alerts/src/CMakeFiles/Alerts.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/Utils/test/Common/CMakeFiles/UtilsCommonTestLib.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/CMakeFiles/gmock_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CertifiedSender/src/CMakeFiles/CertifiedSender.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/src/CMakeFiles/SQLiteStorage.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/RegistrationManager/src/CMakeFiles/RegistrationManager.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
