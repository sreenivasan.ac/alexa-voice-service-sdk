# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/SpeechSynthesizer/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(SpeechSynthesizerTest.testCallingHandleImmediately "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingHandleImmediately")
add_test(SpeechSynthesizerTest.testCallingHandle "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingHandle")
add_test(SpeechSynthesizerTest.testCallingCancel "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingCancel")
add_test(SpeechSynthesizerTest.testCallingCancelAfterHandle "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingCancelAfterHandle")
add_test(SpeechSynthesizerTest.testCallingProvideStateWhenNotPlaying "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingProvideStateWhenNotPlaying")
add_test(SpeechSynthesizerTest.testCallingProvideStateWhenPlaying "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testCallingProvideStateWhenPlaying")
add_test(SpeechSynthesizerTest.testBargeInWhilePlaying "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testBargeInWhilePlaying")
add_test(SpeechSynthesizerTest.testNotCallStopTwice "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testNotCallStopTwice")
add_test(SpeechSynthesizerTest.testMediaPlayerFailedToStop "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testMediaPlayerFailedToStop")
add_test(SpeechSynthesizerTest.testSetStateTimeout "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/test/SpeechSynthesizerTest" "--gtest_filter=SpeechSynthesizerTest.testSetStateTimeout")
