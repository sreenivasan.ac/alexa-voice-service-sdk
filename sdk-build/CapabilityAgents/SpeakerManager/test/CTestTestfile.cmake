# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/SpeakerManager/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(SpeakerManagerTest.testNullContextManager "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testNullContextManager")
add_test(SpeakerManagerTest.testNullMessageSender "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testNullMessageSender")
add_test(SpeakerManagerTest.testNullExceptionSender "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testNullExceptionSender")
add_test(SpeakerManagerTest.testNoSpeakers "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testNoSpeakers")
add_test(SpeakerManagerTest.testContextManagerSetStateConstructor "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testContextManagerSetStateConstructor")
add_test(SpeakerManagerTest.testSetVolumeUnderBounds "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testSetVolumeUnderBounds")
add_test(SpeakerManagerTest.testSetVolumeOverBounds "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testSetVolumeOverBounds")
add_test(SpeakerManagerTest.testAdjustVolumeUnderBounds "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testAdjustVolumeUnderBounds")
add_test(SpeakerManagerTest.testAdjustVolumeOverBounds "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testAdjustVolumeOverBounds")
add_test(SpeakerManagerTest.testSetVolumeOutOfSync "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testSetVolumeOutOfSync")
add_test(SpeakerManagerTest.testAdjustVolumeOutOfSync "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testAdjustVolumeOutOfSync")
add_test(SpeakerManagerTest.testSetMuteOutOfSync "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testSetMuteOutOfSync")
add_test(SpeakerManagerTest.testGetSpeakerSettingsSpeakersOutOfSync "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testGetSpeakerSettingsSpeakersOutOfSync")
add_test(SpeakerManagerTest.testGetConfiguration "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testGetConfiguration")
add_test(SpeakerManagerTest.testAddNullObserver "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testAddNullObserver")
add_test(SpeakerManagerTest.testRemoveSpeakerManagerObserver "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testRemoveSpeakerManagerObserver")
add_test(SpeakerManagerTest.testRemoveNullObserver "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=SpeakerManagerTest.testRemoveNullObserver")
add_test(*/SpeakerManagerTest.testSetVolume/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testSetVolume/*")
add_test(*/SpeakerManagerTest.testAdjustVolume/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testAdjustVolume/*")
add_test(*/SpeakerManagerTest.testSetMute/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testSetMute/*")
add_test(*/SpeakerManagerTest.testGetSpeakerSettings/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testGetSpeakerSettings/*")
add_test(*/SpeakerManagerTest.testSetVolumeDirective/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testSetVolumeDirective/*")
add_test(*/SpeakerManagerTest.testAdjustVolumeDirective/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testAdjustVolumeDirective/*")
add_test(*/SpeakerManagerTest.testSetMuteDirective/* "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeakerManager/test/SpeakerManagerTest" "--gtest_filter=*/SpeakerManagerTest.testSetMuteDirective/*")
