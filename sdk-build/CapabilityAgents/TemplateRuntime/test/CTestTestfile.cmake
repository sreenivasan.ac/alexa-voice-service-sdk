# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/TemplateRuntime/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(TemplateRuntimeTest.testNullAudioPlayerInterface "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testNullAudioPlayerInterface")
add_test(TemplateRuntimeTest.testNullFocusManagerInterface "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testNullFocusManagerInterface")
add_test(TemplateRuntimeTest.testNullExceptionSender "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testNullExceptionSender")
add_test(TemplateRuntimeTest.testAudioPlayerAddRemoveObserver "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testAudioPlayerAddRemoveObserver")
add_test(TemplateRuntimeTest.testUnknownDirective "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testUnknownDirective")
add_test(TemplateRuntimeTest.testRenderTemplateDirective "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderTemplateDirective")
add_test(TemplateRuntimeTest.testHandleDirectiveImmediately "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testHandleDirectiveImmediately")
add_test(TemplateRuntimeTest.testRenderPlayerInfoDirectiveBefore "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderPlayerInfoDirectiveBefore")
add_test(TemplateRuntimeTest.testRenderPlayerInfoDirectiveAfter "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderPlayerInfoDirectiveAfter")
add_test(TemplateRuntimeTest.testRenderPlayerInfoDirectiveWithoutAudioItemId "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderPlayerInfoDirectiveWithoutAudioItemId")
add_test(TemplateRuntimeTest.testMalformedRenderPlayerInfoDirective "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testMalformedRenderPlayerInfoDirective")
add_test(TemplateRuntimeTest.testRenderPlayerInfoDirectiveDifferentAudioItemId "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderPlayerInfoDirectiveDifferentAudioItemId")
add_test(TemplateRuntimeTest.testRenderPlayerInfoDirectiveAudioStateUpdate "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testRenderPlayerInfoDirectiveAudioStateUpdate")
add_test(TemplateRuntimeTest.testFocusNone "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testFocusNone")
add_test(TemplateRuntimeTest.testDisplayCardCleared "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testDisplayCardCleared")
add_test(TemplateRuntimeTest.testReacquireChannel "/Users/h/sdk-folder/sdk-build/CapabilityAgents/TemplateRuntime/test/TemplateRuntimeTest" "--gtest_filter=TemplateRuntimeTest.testReacquireChannel")
