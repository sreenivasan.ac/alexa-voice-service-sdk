# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/Settings/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(SettingsTest.createTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.createTest")
add_test(SettingsTest.addGlobalSettingsObserverTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.addGlobalSettingsObserverTest")
add_test(SettingsTest.removeGlobalSettingsObserverTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.removeGlobalSettingsObserverTest")
add_test(SettingsTest.addSingleSettingObserverWithInvalidKeyTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.addSingleSettingObserverWithInvalidKeyTest")
add_test(SettingsTest.removeSingleSettingObserverWithInvalidKeyTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.removeSingleSettingObserverWithInvalidKeyTest")
add_test(SettingsTest.removeSingleSettingObserverWithCorrectKeyTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.removeSingleSettingObserverWithCorrectKeyTest")
add_test(SettingsTest.defaultSettingsCorrect "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.defaultSettingsCorrect")
add_test(SettingsTest.clearDataTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.clearDataTest")
add_test(SettingsTest.clearDatabaseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.clearDatabaseTest")
add_test(SettingsTest.storeDatabaseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.storeDatabaseTest")
add_test(SettingsTest.modifyDatabaseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.modifyDatabaseTest")
add_test(SettingsTest.eraseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.eraseTest")
add_test(SettingsTest.createDatabaseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.createDatabaseTest")
add_test(SettingsTest.openAndCloseDatabaseTest "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Settings/test/SettingsTest" "--gtest_filter=SettingsTest.openAndCloseDatabaseTest")
