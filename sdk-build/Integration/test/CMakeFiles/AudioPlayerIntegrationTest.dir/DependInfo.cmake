# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Integration/test/AudioPlayerIntegrationTest.cpp" "/Users/h/sdk-folder/sdk-build/Integration/test/CMakeFiles/AudioPlayerIntegrationTest.dir/AudioPlayerIntegrationTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/KWD/include"
  "/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AIP/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/SpeechSynthesizer/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/Alerts/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/System/include"
  "/usr/local/Cellar/gst-plugins-base/1.14.1/include/gstreamer-1.0"
  "/usr/local/Cellar/gstreamer/1.14.1/include/gstreamer-1.0"
  "/usr/local/Cellar/glib/2.56.1/include/glib-2.0"
  "/usr/local/Cellar/glib/2.56.1/lib/glib-2.0/include"
  "/usr/local/opt/gettext/include"
  "/usr/local/Cellar/pcre/8.42/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/MediaPlayer/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/PlaybackController/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/MultipartParser"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ADSL/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ADSL/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AFML/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AIP/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ApplicationUtilities/Resources/Audio/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/RegistrationManager/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AudioPlayer/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AudioPlayer/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Authorization/CBLAuthDelegate/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ContextManager/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ContextManager/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Integration/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googletest/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googlemock/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/SpeechSynthesizer/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/PlaylistParser/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/AIP/src/CMakeFiles/AIP.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/Alerts/src/CMakeFiles/Alerts.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/AudioPlayer/src/CMakeFiles/AudioPlayer.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/System/src/CMakeFiles/AVSSystem.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Integration/src/CMakeFiles/Integration.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/KWD/src/CMakeFiles/KWD.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/PlaybackController/src/CMakeFiles/PlaybackController.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CapabilityAgents/SpeechSynthesizer/src/CMakeFiles/SpeechSynthesizer.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/MediaPlayer/src/CMakeFiles/MediaPlayer.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ADSL/src/CMakeFiles/ADSL.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AFML/src/CMakeFiles/AFML.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ApplicationUtilities/Resources/Audio/src/CMakeFiles/AudioResources.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/CertifiedSender/src/CMakeFiles/CertifiedSender.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Authorization/CBLAuthDelegate/src/CMakeFiles/CBLAuthDelegate.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/src/CMakeFiles/SQLiteStorage.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/RegistrationManager/src/CMakeFiles/RegistrationManager.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ContextManager/src/CMakeFiles/ContextManager.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/PlaylistParser/src/CMakeFiles/PlaylistParser.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
