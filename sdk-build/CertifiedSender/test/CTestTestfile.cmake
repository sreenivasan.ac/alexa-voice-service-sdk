# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test
# Build directory: /Users/h/sdk-folder/sdk-build/CertifiedSender/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(CertifiedSenderTest.clearDataTest "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/CertifiedSenderTest" "--gtest_filter=CertifiedSenderTest.clearDataTest" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testConstructionAndDestruction "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testConstructionAndDestruction" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testDatabaseCreation "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testDatabaseCreation" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testOpenAndCloseDatabase "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testOpenAndCloseDatabase" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testDatabaseStoreAndLoad "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testDatabaseStoreAndLoad" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testDatabaseErase "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testDatabaseErase" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
add_test(MessageStorageTest.testDatabaseClear "/Users/h/sdk-folder/sdk-build/CertifiedSender/test/MessageStorageTest" "--gtest_filter=MessageStorageTest.testDatabaseClear" "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CertifiedSender/test")
