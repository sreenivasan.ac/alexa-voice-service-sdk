# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilitiesDelegate/test/CapabilitiesDelegateTest.cpp" "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CMakeFiles/CapabilitiesDelegateTest.dir/CapabilitiesDelegateTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GSTREAMER_MEDIA_PLAYER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/include"
  "/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilitiesDelegate/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/AVS/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/SDKInterfaces/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/AVSCommon/Utils/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/rapidjson/rapidjson-1.1.0/include"
  "/usr/local/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Authorization/CBLAuthDelegate/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ContextManager/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Integration/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AIP/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/SpeechSynthesizer/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/Alerts/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/AudioPlayer/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilityAgents/System/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/RegistrationManager/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/MultipartParser"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ACL/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ContextManager/src"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googletest/include"
  "/Users/h/sdk-folder/sdk-source/avs-device-sdk/ThirdParty/googletest-release-1.8.0/googlemock/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/src/CMakeFiles/CapabilitiesDelegate.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/CMakeFiles/gmock_main.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Integration/src/CMakeFiles/Integration.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ACL/src/CMakeFiles/ACL.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Authorization/CBLAuthDelegate/src/CMakeFiles/CBLAuthDelegate.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/src/CMakeFiles/SQLiteStorage.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ContextManager/src/CMakeFiles/ContextManager.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/RegistrationManager/src/CMakeFiles/RegistrationManager.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/AVSCommon/CMakeFiles/AVSCommon.dir/DependInfo.cmake"
  "/Users/h/sdk-folder/sdk-build/ThirdParty/googletest-release-1.8.0/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
