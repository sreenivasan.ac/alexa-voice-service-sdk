# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/CapabilitiesDelegate/test
# Build directory: /Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(CapabilitiesDelegateTest.noCapability "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.noCapability")
add_test(CapabilitiesDelegateTest.withCapabilitiesHappyCase "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.withCapabilitiesHappyCase")
add_test(CapabilitiesDelegateTest.publishFatalError "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.publishFatalError")
add_test(CapabilitiesDelegateTest.publishRetriableError "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.publishRetriableError")
add_test(CapabilitiesDelegateTest.republish "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.republish")
add_test(CapabilitiesDelegateTest.registerTests "/Users/h/sdk-folder/sdk-build/CapabilitiesDelegate/test/CapabilitiesDelegateTest" "--gtest_filter=CapabilitiesDelegateTest.registerTests")
