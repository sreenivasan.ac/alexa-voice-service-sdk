# CMake generated Testfile for 
# Source directory: /Users/h/sdk-folder/sdk-source/avs-device-sdk/Storage/SQLiteStorage/test
# Build directory: /Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(SQLiteDatabaseTest.CloseThenOpen "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.CloseThenOpen" ".")
add_test(SQLiteDatabaseTest.InitializeAlreadyExisting "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.InitializeAlreadyExisting" ".")
add_test(SQLiteDatabaseTest.InitializeBadPath "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.InitializeBadPath" ".")
add_test(SQLiteDatabaseTest.InitializeOnDirectory "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.InitializeOnDirectory" ".")
add_test(SQLiteDatabaseTest.InitializeTwice "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.InitializeTwice" ".")
add_test(SQLiteDatabaseTest.OpenAlreadyExisting "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.OpenAlreadyExisting" ".")
add_test(SQLiteDatabaseTest.OpenBadPath "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.OpenBadPath" ".")
add_test(SQLiteDatabaseTest.OpenDirectory "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.OpenDirectory" ".")
add_test(SQLiteDatabaseTest.OpenTwice "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteDatabaseTest" "--gtest_filter=SQLiteDatabaseTest.OpenTwice" ".")
add_test(SQLiteMiscStorageTest.createStringKeyValueTable "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteMiscStorageTest" "--gtest_filter=SQLiteMiscStorageTest.createStringKeyValueTable" ".")
add_test(SQLiteMiscStorageTest.tableEntryTests "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteMiscStorageTest" "--gtest_filter=SQLiteMiscStorageTest.tableEntryTests" ".")
add_test(SQLiteMiscStorageTest.loadAndClear "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteMiscStorageTest" "--gtest_filter=SQLiteMiscStorageTest.loadAndClear" ".")
add_test(SQLiteMiscStorageTest.createDeleteTable "/Users/h/sdk-folder/sdk-build/Storage/SQLiteStorage/test/SQLiteMiscStorageTest" "--gtest_filter=SQLiteMiscStorageTest.createDeleteTable" ".")
